package app.web.sleepcoderepeat.marecotest.screen.main

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.marecotest.data.MainDataLocalSource
import app.web.sleepcoderepeat.marecotest.data.local.DataLocalSource
import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity
import app.web.sleepcoderepeat.marecotest.databinding.GalleryItemBinding
import app.web.sleepcoderepeat.marecotest.utils.FileUtils
import app.web.sleepcoderepeat.marecotest.utils.SingleLiveEvent
import app.web.sleepcoderepeat.marecotest.utils.getImageDir
import app.web.sleepcoderepeat.marecotest.utils.getVideoDir
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

class MainViewModel(application: Application): AndroidViewModel(application) {
    lateinit var dataLocalSource: DataLocalSource
    val listGallery: ObservableArrayList<ImageGalleryEntity> = ObservableArrayList()
    val message = MutableLiveData<String>()
    val isLoading = MutableLiveData<Boolean>()
    val context = getApplication<Application>()
    internal val openPreview = SingleLiveEvent<ItemClickObject>()
    internal val showOption = MutableLiveData<ImageGalleryEntity>()

    fun init() {
        isLoading.value = false
        dataLocalSource = DataLocalSource(context)

        getAllGallery()
    }

    fun openPreview(galeryBinding: GalleryItemBinding, imageGalleryEntity: ImageGalleryEntity) {
        openPreview.value = ItemClickObject(galeryBinding, imageGalleryEntity)
    }

    fun showOption(imageGalleryEntity: ImageGalleryEntity) {
        showOption.value = imageGalleryEntity
    }

    fun addImageFromGallery(path: String) {
        val newFileName = "marecotest-${System.currentTimeMillis()}.png"
        val bitmap = FileUtils().getCompressedImage(path)
        bitmap?.let {
            FileUtils().saveBitmapToFile(
                context.getImageDir(),
                newFileName,
                it,
                Bitmap.CompressFormat.PNG,
                100
            )
            addToGallery(newFileName, context.getImageDir().path + File.separator + newFileName)
        }
    }

    fun addImageFromCamera(bitmap: Bitmap) {
        val newFileName = "marecotest-${System.currentTimeMillis()}.png"
        bitmap.let {
            FileUtils().saveBitmapToFile(
                context.getImageDir(),
                newFileName,
                it,
                Bitmap.CompressFormat.PNG,
                100
            )
            addToGallery(newFileName, context.getImageDir().path + File.separator + newFileName)
        }
    }

    fun addVideoFromCamera(inpustream: InputStream) {
        val newFileName = "marecotest-${System.currentTimeMillis()}.mp4"
        FileUtils().saveFileFromInputStream(inpustream, newFileName, context.getVideoDir())
            ?.let {
                addToGallery(newFileName, context.getVideoDir().path + File.separator + newFileName)
            }

    }

    private fun addToGallery(filename: String, path: String) {

        dataLocalSource.add(
            ImageGalleryEntity(
                name = filename,
                path = path
            ),
            object : MainDataLocalSource.AddImageGallery {
                override fun onSuccess(id: Long?) {
                    message.value = "File Added"
                }

                override fun onError(msg: String?) {
                    message.value = "Added failed $msg"
                }
            }
        )
    }

    fun addVideoFromGallery(path: String) {
        val newFileName = "marecotest-${System.currentTimeMillis()}.mp4"
        val inputStream: InputStream = FileInputStream(path)
        FileUtils().saveFileFromInputStream(
            inputStream,
            newFileName,
            context.getVideoDir()
        )?.let {
            addToGallery(newFileName, context.getVideoDir().path + File.separator + newFileName)
        }

    }

    fun getAllGallery() {
        dataLocalSource.showAll(object : MainDataLocalSource.GetAllImageGallery {
            override fun onSuccess(list: List<ImageGalleryEntity>) {
                listGallery.apply {
                    clear()
                    addAll(list)
                }
                Log.e("success", list.toString())
            }

            override fun onError(msg: String?) {
                msg?.let {
                    Log.e("Error", msg)
                }
            }

            override fun onNotAvailable() {
                Log.e("notavail", "zero")
            }
        })
    }

    fun deleteClicked(list: List<Int>) {
        dataLocalSource.delete(list,
            object : MainDataLocalSource.DeleteImagGallery {
                override fun onSuccess() {
                    message.value = "Delete Success"
                }

                override fun onError(msg: String?) {
                    message.value = "Delete Failed"
                }
            })
    }

}