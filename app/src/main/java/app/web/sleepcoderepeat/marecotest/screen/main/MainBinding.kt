package app.web.sleepcoderepeat.marecotest.screen.main

import android.app.Activity
import android.util.DisplayMetrics
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity
import app.web.sleepcoderepeat.marecotest.utils.getUri
import app.web.sleepcoderepeat.marecotest.utils.load

object MainBinding {

    @BindingAdapter("loadImageFromPath")
    @JvmStatic
    fun loadImageFromPath(imageview: ImageView, path: String){
        imageview.load(path.getUri())
    }

    @BindingAdapter("listGallery")
    @JvmStatic
    fun setListGallery(recyclerView: RecyclerView, list: MutableList<ImageGalleryEntity>){
        if (list.size != 0)
            (recyclerView.adapter as MainAdapter).replaceData(list)
    }
}

