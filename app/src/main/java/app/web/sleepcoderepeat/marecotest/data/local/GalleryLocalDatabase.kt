package app.web.sleepcoderepeat.marecotest.data.local

import android.content.Context
import android.graphics.Bitmap
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import app.web.sleepcoderepeat.marecotest.data.local.dao.ImageGalleryDao
import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity
import app.web.sleepcoderepeat.marecotest.utils.FileUtils
import app.web.sleepcoderepeat.marecotest.utils.getImageDir
import app.web.sleepcoderepeat.marecotest.utils.getVideoDir
import app.web.sleepcoderepeat.marecotest.utils.ioThread
import java.io.File


@Database(
    entities = [ImageGalleryEntity::class],
    version = 1,
    exportSchema = false
)
abstract class GalleryLocalDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "gallerymareco.db"
        private var INSTANCE: GalleryLocalDatabase? = null
        private fun create(context: Context): GalleryLocalDatabase =
            Room.databaseBuilder(context, GalleryLocalDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        ioThread {
                            getInstance(context).imageGalleryDao().bulkAddData(
                                getPrePopulateData(
                                    context
                                )
                            )
                        }
                    }
                })
                .build()

        fun getPrePopulateData(context: Context): List<ImageGalleryEntity> {
            val bitmapAssets = listOf(
                BitmapAssets("satu", getBitmap(context, "satu.png")),
                BitmapAssets("dua", getBitmap(context, "dua.png")),
                BitmapAssets("tiga", getBitmap(context, "tiga.png")),
                BitmapAssets("empat", getBitmap(context, "empat.png")),
                BitmapAssets("lima", getBitmap(context, "lima.png")),
                BitmapAssets("enam", getBitmap(context, "enam.png")),
                BitmapAssets("tujuh", getBitmap(context, "tujuh.png"))
            )
            val imageDir = context.getImageDir()
            var doSaveImageDir = true
            if (!imageDir.exists()) {
                doSaveImageDir = imageDir.mkdirs()
            }
            val list: ArrayList<ImageGalleryEntity> = arrayListOf()
            for (bitmapItem in bitmapAssets) {
                if (doSaveImageDir) {
                    bitmapItem.bitmap?.let {
                        FileUtils().saveBitmapToFile(
                            imageDir,
                            bitmapItem.name + ".png",
                            it,
                            Bitmap.CompressFormat.PNG,
                            100
                        )
                    }
                    list.add(
                        ImageGalleryEntity(
                            name = bitmapItem.name,
                            path = imageDir.path + File.separator + bitmapItem.name + ".png"
                        )
                    )
                }
            }
            val videoDir = context.getVideoDir()
            var doSaveVideoDir = true
            if (!videoDir.exists()) {
                doSaveVideoDir = videoDir.mkdirs()
            }
            if (doSaveVideoDir) {
                FileUtils().saveFileFromAsset(context, "video.mp4", "video.mp4", videoDir)
                list.add(
                    ImageGalleryEntity(
                        name = "video",
                        path = videoDir.path + File.separator + "video.mp4"
                    )
                )
            }
            return list
        }

        private fun getBitmap(context: Context, path: String): Bitmap? =
            FileUtils().getBitmapFromAsset(
                context,
                path
            )

        fun getInstance(context: Context): GalleryLocalDatabase =
            (INSTANCE ?: synchronized(this) {
                INSTANCE ?: create(context).also { INSTANCE = it }
            })
    }

    abstract fun imageGalleryDao(): ImageGalleryDao

    data class BitmapAssets(val name: String, val bitmap: Bitmap?)
}