package app.web.sleepcoderepeat.marecotest.screen.main

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.marecotest.R
import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity
import app.web.sleepcoderepeat.marecotest.databinding.GalleryItemBinding
import app.web.sleepcoderepeat.marecotest.utils.FileUtils
import app.web.sleepcoderepeat.marecotest.utils.getUri

class MainAdapter(
    private var list: MutableList<ImageGalleryEntity>,
    private var mainViewModel: MainViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ImageGalleryHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.gallery_item,
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ImageGalleryHolder).apply {
            bindItem(list[position])
            binding.rvArea.setOnClickListener {
                mainViewModel.openPreview(holder.binding, list[position])
            }
            binding.rvArea.setOnLongClickListener {
                mainViewModel.showOption(list[position])
                true
            }
        }
    }

    override fun getItemCount(): Int = list.size

    fun replaceData(items: MutableList<ImageGalleryEntity>) {
        setList(items)
    }

    private fun setList(item: MutableList<ImageGalleryEntity>) {
        this.list = item
        notifyDataSetChanged()
    }

    class ImageGalleryHolder(var binding: GalleryItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bindItem(imageGalleryEntity: ImageGalleryEntity) {
            val dm = DisplayMetrics()
            (binding.root.context as Activity).windowManager.defaultDisplay.getMetrics(dm)
            binding.imgGallery.apply {
                layoutParams.width = getSizeByScreen(dm)
                layoutParams.height = getSizeByScreen(dm)
            }
            binding.overlayViewColor.apply {
                layoutParams.width = getSizeByScreen(dm)
                layoutParams.height = getSizeByScreen(dm)
            }

            binding.playIcon.visibility = View.GONE
            imageGalleryEntity.path?.getUri()?.let {
                FileUtils().getMimeType(binding.root.context, it)?.apply {
                    if (this.contains("video", true)) {
                        binding.playIcon.visibility = View.VISIBLE
                    }
                }
            }
            binding.gallery = imageGalleryEntity
            binding.executePendingBindings()
        }

        private fun getSizeByScreen(dm: DisplayMetrics) : Int{
            return when {
                dm.widthPixels in 1001..1999 -> {
                    dm.widthPixels/4
                }
                dm.widthPixels > 2000 -> {
                    dm.widthPixels/8
                }
                else -> {
                    dm.widthPixels/3
                }
            }
        }
    }
}