package app.web.sleepcoderepeat.marecotest.utils

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.web.sleepcoderepeat.marecotest.R
import com.bumptech.glide.Glide
import java.io.File


fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelCLass: Class<T>)
= ViewModelProvider(this, ViewModelFactory(application)).get(viewModelCLass)

fun ImageView.load(url: Uri){
    Glide.with(context)
        .load(url)
        .thumbnail(0.1f)
        .placeholder(R.mipmap.ic_launcher_round)
        .error(R.drawable.ic_baseline_error_24)
        .into(this)
}

fun String.getUri(): Uri {
    return Uri.fromFile(File(this))
}

fun Context.getImageDir(): File = File(
    this.getExternalFilesDir(null)?.absolutePath +
            File.separator + "images"
)

fun Context.getVideoDir(): File = File(
    this.getExternalFilesDir(null)?.absolutePath +
            File.separator + "videos"
)