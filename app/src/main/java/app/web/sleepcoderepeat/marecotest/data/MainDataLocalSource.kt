package app.web.sleepcoderepeat.marecotest.data

import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity

interface MainDataLocalSource {
    fun showAll(callback: GetAllImageGallery)
    fun showSingle(id: Int, callback: GetSingleImageGallery)
    fun add(imageGallery: ImageGalleryEntity, callback: AddImageGallery)
    fun update(nameUpdate: String, id: Int, callback: UpdateImageGallery)
    fun delete(id: List<Int>, callback: DeleteImagGallery)



    interface GetAllImageGallery{
        fun onSuccess(list: List<ImageGalleryEntity>)
        fun onError(msg: String?)
        fun onNotAvailable()
    }

    interface GetSingleImageGallery{
        fun onSuccess(imageGallery: ImageGalleryEntity)
        fun onError(msg: String?)
        fun onNotAvailable()
    }

    interface AddImageGallery{
        fun onSuccess(id: Long?)
        fun onError(msg: String?)
    }

    interface UpdateImageGallery{
        fun onSuccess()
        fun onError(msg: String?)
    }

    interface DeleteImagGallery{
        fun onSuccess()
        fun onError(msg: String?)
    }
}