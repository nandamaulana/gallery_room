package app.web.sleepcoderepeat.marecotest.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ImageGalleryDao {
    @Insert(onConflict = REPLACE)
    fun addImage(imageGallery: ImageGalleryEntity) : Single<Long>

    @Insert
    fun bulkAddData(data: List<ImageGalleryEntity>)

    @Query("SELECT * FROM gallery")
    fun getListImageGallery(): Flowable<List<ImageGalleryEntity>>

    @Query("SELECT * FROM gallery where id = :id")
    fun getSingleImageGallery(id: Int): Flowable<ImageGalleryEntity>

    @Query("UPDATE gallery SET name = :nameUpdate where id = :id")
    fun updateGalleryName(nameUpdate: String, id: Int) : Single<Int>

    @Query("DELETE FROM gallery WHERE id = (:idList)")
    fun deleteImageGalleryByList(idList: List<Int>) : Single<Int>
}