package app.web.sleepcoderepeat.marecotest.screen.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.util.Pair
import app.web.sleepcoderepeat.marecotest.R
import app.web.sleepcoderepeat.marecotest.databinding.ActivityMainBinding
import app.web.sleepcoderepeat.marecotest.screen.preview.PreviewActivity
import app.web.sleepcoderepeat.marecotest.utils.*
import java.io.FileInputStream
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding

    private val rotateOpen: Animation by lazy {
        AnimationUtils.loadAnimation(
            this,
            R.anim.rotate_open_anim
        )
    }
    private val rotateClose: Animation by lazy {
        AnimationUtils.loadAnimation(
            this,
            R.anim.rotate_close_anim
        )
    }
    private val fromBottom: Animation by lazy {
        AnimationUtils.loadAnimation(
            this,
            R.anim.from_bottom_anim
        )
    }
    private val toBottom: Animation by lazy {
        AnimationUtils.loadAnimation(
            this,
            R.anim.to_bottom_anim
        )
    }

    private var isClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(MainViewModel::class.java)
        }
        mainBinding.lifecycleOwner = this
        setContentView(mainBinding.root)
        windowManager.defaultDisplay
        setUpObserver()
        setUpFabAction()
        setUpRecyclerView()
        requestPermissions()
    }


    private fun setUpRecyclerView() {
        mainBinding.vm?.let {
            mainBinding.rvGallery.adapter = MainAdapter(it.listGallery, it)
        }
    }

    private fun requestPermissions() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(
                    applicationContext,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) ==
                PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
                requestPermissions(permissions, FIRST_RUN_PERMISSION)
            }
        }
    }

    private fun setUpFabAction() {
        mainBinding.apply {
            mainFab.setOnClickListener {
                mainFabButtonClick()
            }
            galleryFab.setOnClickListener {
                if (isMarshmallowAbove()) {
                    if (isPermissionDenied(
                            applicationContext,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                        || isPermissionDenied(
                            applicationContext,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                    ) {
                        val permissions = arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        requestPermissions(permissions, PERMISSION_GALLERY_CODE)
                        return@setOnClickListener
                    }
                }
                pickFromGallery()

            }
            cameraFab.setOnClickListener {
                if (isMarshmallowAbove()) {
                    if (isPermissionDenied(applicationContext, Manifest.permission.CAMERA)
                    ) {
                        val permissions = arrayOf(Manifest.permission.CAMERA)
                        requestPermissions(permissions, PERMISSION_CAMERA_CODE)
                        return@setOnClickListener
                    }
                }
                pickImageFromCamera()
            }
            videoFab.setOnClickListener {
                if (isMarshmallowAbove()) {
                    if (isPermissionDenied(applicationContext, Manifest.permission.CAMERA)
                    ) {
                        val permissions = arrayOf(Manifest.permission.CAMERA)
                        requestPermissions(permissions, PERMISSION_CAMERA_CODE)
                        return@setOnClickListener
                    }
                }
                pickVideoFromCamera()
            }
        }
    }

    private fun mainFabButtonClick() {
        setVisibility(isClicked)
        setAnimation(isClicked)
        isClicked = !isClicked
    }

    private fun setAnimation(boolean: Boolean) {
        if (!boolean) {
            mainBinding.apply {
                mainFab.startAnimation(rotateOpen)
                cameraFab.startAnimation(fromBottom)
                galleryFab.startAnimation(fromBottom)
                videoFab.startAnimation(fromBottom)
            }
        } else {
            mainBinding.apply {
                mainFab.startAnimation(rotateClose)
                cameraFab.startAnimation(toBottom)
                galleryFab.startAnimation(toBottom)
                videoFab.startAnimation(toBottom)
            }
        }
    }

    private fun setVisibility(boolean: Boolean) {
        if (!boolean) {
            mainBinding.apply {
                cameraFab.visibility = View.VISIBLE
                galleryFab.visibility = View.VISIBLE
                videoFab.visibility = View.VISIBLE
            }
        } else {
            mainBinding.apply {
                cameraFab.visibility = View.GONE
                galleryFab.visibility = View.GONE
                videoFab.visibility = View.GONE
            }
        }

    }

    private fun setUpObserver() {
        mainBinding.vm?.apply {
            message.observe(this@MainActivity, {
                Toast.makeText(this@MainActivity, it, Toast.LENGTH_SHORT).show()
            })

            openPreview.observe(this@MainActivity, { itPreview ->
                val intent = Intent(this@MainActivity, PreviewActivity::class.java)
                FileUtils().getFile(context, itPreview.imageGalleryEntity.path?.getUri())?.let {
                    FileUtils().getMimeType(it)?.apply {
                        intent.putExtra(
                            KEY_TYPE,
                            if (this.contains("video", true)) TYPE_VIDEO else TYPE_IMAGE
                        )
                    }
                }
                intent.putExtra(KEY_PATH, itPreview.imageGalleryEntity.path)
                val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this@MainActivity,
                    Pair.create(itPreview.galleryItemBinding.imgGallery, TRANSITION_IMG_KEY)
                ).toBundle()
                startActivity(intent, option)
            })

            showOption.observe(this@MainActivity, {
                val builder = AlertDialog.Builder(this@MainActivity)
                builder.setMessage(getString(R.string.delete_item))
                    .setPositiveButton(
                        getString(R.string.delete)
                    ) { _, _ ->
                        val list = listOf<Int>(it.id)
                        this.deleteClicked(list)
                    }
                    .setNegativeButton(
                        getString(R.string.cancel)
                    ) { _, _ ->
                    }
                // Create the AlertDialog object and return it
                builder.create()
                builder.show()
            })

            init()
        }

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_GALLERY_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] !=
                    PackageManager.PERMISSION_GRANTED
                ) {
                    pickFromGallery()
                } else {
                    Toast.makeText(applicationContext, "Permission denied", Toast.LENGTH_SHORT)
                        .show()

                }
            }
            PERMISSION_CAMERA_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] !=
                    PackageManager.PERMISSION_GRANTED
                ) {
                    pickImageFromCamera()
                } else {
                    Toast.makeText(applicationContext, "Permission denied", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            PERMISSION_CAMERA_VIDEO_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] !=
                    PackageManager.PERMISSION_GRANTED
                ) {
                    pickVideoFromCamera()
                } else {
                    Toast.makeText(applicationContext, "Permission denied", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                PICK_IMAGE_GALLERY -> {
                    FileUtils().getFile(
                        applicationContext,
                        data?.data
                    )?.apply {
                        FileUtils().getMimeType(this)?.let {
                            if (it.contains("video", true)) {
                                Log.e("image from gallery", this.path)
                                mainBinding.vm?.addVideoFromGallery(this.path)
                            } else {
                                mainBinding.vm?.addImageFromGallery(this.path)
                            }
                        }
                    }
                }
                PICK_IMAGE_CAMERA -> {
                    mainBinding.vm?.addImageFromCamera(data?.extras?.get("data") as Bitmap)
                }
                PICK_VIDEO_CAMERA -> {
                    FileUtils().getFile(
                        applicationContext,
                        data?.data
                    )?.path?.let {
                        mainBinding.vm?.addVideoFromCamera(FileInputStream(it) as InputStream)
                    }
                }
            }
    }

    override fun onResume() {
        super.onResume()
        mainBinding.vm?.getAllGallery()
    }

    private fun pickFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/* video/*"
        startActivityForResult(intent, PICK_IMAGE_GALLERY)
    }

    private fun pickImageFromCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intentImage ->
            intentImage.resolveActivity(packageManager)?.let {
                startActivityForResult(intentImage, PICK_IMAGE_CAMERA)
            }
        }
    }

    private fun pickVideoFromCamera() {
        Intent(MediaStore.ACTION_VIDEO_CAPTURE).also { intentImage ->
            intentImage.resolveActivity(packageManager)?.let {
                startActivityForResult(intentImage, PICK_VIDEO_CAMERA)
            }
        }
    }

    companion object {
        private const val FIRST_RUN_PERMISSION = 500
        private const val PERMISSION_GALLERY_CODE = 100
        private const val PERMISSION_CAMERA_CODE = 150
        private const val PERMISSION_CAMERA_VIDEO_CODE = 160
        private const val PICK_IMAGE_GALLERY = 200
        private const val PICK_IMAGE_CAMERA = 300
        private const val PICK_VIDEO_CAMERA = 350

    }
}