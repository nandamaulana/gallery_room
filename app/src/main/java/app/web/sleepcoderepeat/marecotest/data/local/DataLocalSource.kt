package app.web.sleepcoderepeat.marecotest.data.local

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import app.web.sleepcoderepeat.marecotest.data.MainDataLocalSource
import app.web.sleepcoderepeat.marecotest.data.local.entity.ImageGalleryEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DataLocalSource(context: Context) : MainDataLocalSource {

    private val imageGalleryDao = GalleryLocalDatabase.getInstance(context).imageGalleryDao()

    override fun showAll(callback: MainDataLocalSource.GetAllImageGallery) {
        imageGalleryDao.apply {
            getListImageGallery()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t ->
                    if (t != null)
                        callback.onSuccess(t)
                    else
                        callback.onNotAvailable()
                }, { t: Throwable? ->
                    callback.onError(t?.localizedMessage)
                })
        }
    }

    override fun showSingle(id: Int, callback: MainDataLocalSource.GetSingleImageGallery) {
        imageGalleryDao.apply {
            getSingleImageGallery(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t ->
                    if (t != null)
                        callback.onSuccess(t)
                    else
                        callback.onNotAvailable()
                }, { t: Throwable? ->
                    callback.onError(t?.localizedMessage)
                })
        }
    }

    override fun add(
        imageGallery: ImageGalleryEntity,
        callback: MainDataLocalSource.AddImageGallery
    ) {
        imageGalleryDao.apply {
            addImage(imageGallery)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ t ->
                if (t != null)
                    callback.onSuccess(t)
            }, { t: Throwable? ->
                callback.onError(t?.localizedMessage)
            })
        }
    }

    override fun update(
        nameUpdate: String,
        id: Int,
        callback: MainDataLocalSource.UpdateImageGallery
    ) {
        imageGalleryDao.apply {
            updateGalleryName(nameUpdate, id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t ->
                    if (t != null)
                        callback.onSuccess()
                }, { t: Throwable? ->
                    callback.onError(t?.localizedMessage)
                })
        }
    }

    override fun delete(id: List<Int>, callback: MainDataLocalSource.DeleteImagGallery) {
        imageGalleryDao.apply {
            deleteImageGalleryByList(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ t ->
                if (t != null)
                    callback.onSuccess()
            }, { t: Throwable? ->
                callback.onError(t?.localizedMessage)
            })
        }
    }
}